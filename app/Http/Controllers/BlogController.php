<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use App\Image;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = Blog::all();
        return view('admin.blog.index', compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $path="uploads/noimage.jpg";
        $input = $request->all();
        $blog = new Blog;
        $blog->title = $input['title'];
        $blog->detail = $input['detail'];

        if($request->hasFile('cover_img')){
            $file = $request->file('cover_img');
            $img_name = $file->getClientOriginalName();
            $file->move('uploads/blogs', $img_name);
            $path = "/uploads/blogs/".$img_name;
        }else{
            return redirect()->back();
        }
        if($request->hasFile('images')){
            $blog->cover_img = $path;
            $blog->save();

            $images = $request->file('images');
            foreach($images as $image){
                $img = new Image;
                $name = $image->getClientOriginalName();
                $image->move('uploads/blogs', $name);
                $path = "/uploads/blogs/".$name;
                $img->blog_id = $blog->id;
                $img->image = $path;
                $img->save();
            }
        }
        else{
            return redirect()->back();
        }

        return redirect('backend/blog');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog = Blog::find($id);
        return view('admin.blog.edit', compact('blog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $blog = Blog::find($id);
        $blog->title = $input['title'];
        $blog->detail = $input['detail'];
        $blog->save();

        return redirect('backend/blog');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog = Blog::find($id);
        $blog->delete();
        return redirect('backend/blog');
    }
}
