<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Member;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $members = Member::all();
        return view('admin.member.index', compact('members'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.member.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $path="uploads/noimage.jpg";
        
        $member = new Member;
        $member->name = $input['name'];
        $member->position = $input['position'];
        $member->email = $input['email'];
        $member->web = $input['web'];
        if($request->hasFile('image')){
            $file = $request->file('image');
            $img_name = $file->getClientOriginalName();
            $file->move('uploads/instructors', $img_name);
            $path = "/uploads/instructors/".$img_name;
            // $assfile = $request->file->storeAs('public/upload', $filename);
        }
        $member->image = $path;
        $member->save();

        return redirect('backend/member');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $member = Member::find($id);
        return view('admin.member.edit', compact('member'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        
        $member = Member::find($id);
        $member->name = $input['name'];
        $member->position = $input['position'];
        $member->email = $input['email'];
        $member->web = $input['web'];
        $member->save();

        return redirect('backend/member');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
