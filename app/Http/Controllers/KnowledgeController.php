<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Knowledge;
use App\Icon;

class KnowledgeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $knowledges = Knowledge::all();
        return view('admin.knowledge.index', compact('knowledges'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.knowledge.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $iid = Icon::where('name', $input['icon'])->first()->id;
  
        $knowledge = new Knowledge;
        $knowledge->title = $input['title'];
        $knowledge->detail = $input['detail'];
        $knowledge->icon_id = $iid;
        if($request->hasFile('cover')){
            $file = $request->file('cover');
            $img_name = $file->getClientOriginalName();
            $file->move('uploads/knowledges', $img_name);
            $path = "/uploads/knowledges/".$img_name;
        }
        $knowledge->cover = $path;
        $knowledge->credit = $input['credit'];
        $knowledge->save();

        return redirect('backend/knowledge');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $knowledge = Knowledge::find($id);
        return view('admin.knowledge.edit', compact('knowledge'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

        $iid = Icon::where('name', $input['icon'])->first()->id;

        $knowledge = Knowledge::find($id);
        $knowledge->title = $input['title'];
        $knowledge->detail = $input['detail'];
        $knowledge->credit = $input['credit'];
        $knowledge->icon_id = $iid;
        $knowledge->save();

        return redirect('backend/knowledge');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $knowledge = Knowledge::find($id);
        $knowledge->delete();
        return redirect('backend/knowledge');
    }
}
