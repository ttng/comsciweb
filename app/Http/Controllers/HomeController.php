<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Member;
use App\Blog;
use App\Paper;
use App\Knowledge;
use App\Gallery;

class HomeController extends Controller
{
    public function index(){
        $head = Member::where('position', 2)->first();
        $teams = Member::where('position', 1)->get();

        $blogs = Blog::take(9)->get();
        $papers = Paper::take(6)->get();
        $galleries = Gallery::take(6)->get();

        return view('home', compact(
            'head',
            'teams',
            'blogs',
            'papers',
            'galleries'
        ));
    }

    public function getKnowledge(){
        $knowledges = Knowledge::all();
        return view('knowledges.knowledge', compact('knowledges'));
    }
    public function showKnowledge($id){
        $knowledge = Knowledge::find($id);
        return view('knowledges.showknowledge', compact('knowledge'));
    }

    public function showBlog($id){
        $blog = Blog::find($id);
        return view('blogs.showblog', compact('blog'));
    }

    public function getBlog(){
        $blogs = Blog::all();
        return view('blogs.allblog', compact('blogs'));
    }

    public function getPaper(){
        $papers = Paper::all();
        return view('paper.paper', compact('papers'));
    }
    public function showPaper($id){
        $paper = Paper::find($id);
        return view('paper.showpaper', compact('paper'));
    }

    public function getGallery(){
        $galleries = Gallery::all();
        return view('gallery.index', compact('galleries'));
    }
}
