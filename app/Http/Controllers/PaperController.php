<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Paper;

class PaperController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $papers = Paper::all();
        return view('admin.paper.index', compact('papers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.paper.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $path="uploads/noimage.jpg";
        
        $paper = new Paper;
        $paper->title = $input['title'];
        $paper->author = $input['author'];
        $paper->detail = $input['detail'];
        if($request->hasFile('cover')){
            $file = $request->file('cover');
            $img_name = $file->getClientOriginalName();
            $file->move('uploads/papers', $img_name);
            $path = "/uploads/papers/".$img_name;
            // $assfile = $request->file->storeAs('public/upload', $filename);
        }
        $paper->cover = $path;
        $paper->save();

        return redirect('backend/paper');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $paper = Paper::find($id);
        return view('admin.paper.edit', compact('paper'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        
        $paper = Paper::find($id);
        $paper->title = $input['title'];
        $paper->author = $input['author'];
        $paper->detail = $input['detail'];
        $paper->save();

        return redirect('backend/paper');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
