<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class AuthController extends Controller
{
    public function login(){
        
        return view('auth.login');
    }

    public function register(){
        
        return view('auth.register');
    }

    public function postLogin(Request $request){
        $input = $request->all();
        $uname = $input['username'];
        $user = User::where('name', $uname)->first();

        if($user){
            if($user->password == $input['password']){
                Auth::login($user);
                return redirect('backend');
            }
            else{
                return redirect()->back();
            }
        }else{
            return redirect()->back();
        }
    }

    public function storeUser(Request $request){
        $inputs = $request->all();
        $user = new User;
        $user->name = $inputs['username'];
        $user->email = $inputs['email'];
        $user->password = $inputs['password'];
        $user->save();

        return redirect('/backend');
    }

    public function getLogout(){
        Auth::logout();
        return redirect('/');
    }
}
