<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });
Route::get('/', 'HomeController@index');
Route::get('login', 'AuthController@login');
Route::post('postlogin', 'AuthController@postLogin');

Route::get('knowledge', 'HomeController@getKnowledge');
Route::get('knowledge/{id}', 'HomeController@showKnowledge');

Route::get('paper', 'HomeController@getPaper');
Route::get('paper/{id}', 'HomeController@showPaper');

Route::get('showblog/{id}', 'HomeController@showBlog');
Route::get('getblog', 'HomeController@getBlog');

Route::get('gallery', 'HomeController@getGallery');

Route::group(['prefix' => 'backend', 'middleware'=>'admin-auth'], function() {
    
    Route::get('register', 'AuthController@register');
    Route::post('storeuser', 'AuthController@storeUser');
    Route::get('getlogout', 'AuthController@getLogout');
    
    Route::get('/', 'UserController@index');
    Route::resource('blog', 'BlogController');
    Route::resource('member', 'MemberController');
    Route::resource('activity', 'ActivityController');
    Route::resource('paper', 'PaperController');
    Route::resource('gallery', 'GalleryController');
    Route::resource('knowledge', 'KnowledgeController');
});
