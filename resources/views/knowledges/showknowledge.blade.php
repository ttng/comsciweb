@extends('master')

@section('style')
<style>
.blog-content{
  margin-bottom: 6vh;
}
.add-icon{
    color: #1dc8cd;
}
</style>
@endsection

@section('content')
<section id="portfolio" class="clearfix">
    <div class="row">
        <div class="col-md-12" style="height:10vh"></div>
    </div>
    <div class="container">
      <div class="row blog-content">
        <div class="col-md-6">
          <img src="{{$knowledge->cover}}" alt="" style="width:100%;">
        </div>
        <div class="col-md-6">
          <header class="section-header">
            <?php
              $icon_name = App\Icon::find($knowledge->icon_id)->name;
            ?>
            <h3 class="section-title"><i class="fa {{$icon_name}} add-icon"></i> {{$knowledge->title}}</h3>
          </header>
          <div>
            {{$knowledge->detail}}
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 text-right">
          Credit : <a href="{{$knowledge->credit}}" target="_blank">{{str_limit($knowledge->credit, 50)}}</a>
        </div>
      </div>
    </div>
</section>
@endsection