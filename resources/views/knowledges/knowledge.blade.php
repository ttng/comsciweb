@extends('master')

@section('content')
    <!--==========================
      Why Us Section
    ============================-->
    <section id="why-us" class="wow fadeIn">
        <div class="row">
            <div class="col-md-12" style="height:10vh"></div>
        </div>
        <div class="container">
            <header class="section-header">
                <h3>Knowledge</h3>
            </header>
    
            <div class="row row-eq-height justify-content-center">
                @foreach($knowledges as $knowledge)
                <div class="col-lg-4 mb-4">
                    <div class="card wow bounceInUp">
                        <?php
                            $icon = App\Icon::where('id', $knowledge->icon_id)->first();
                        ?>
                        <i class="fa {{$icon->name}}"></i>
                    <div class="card-body">
                        <h5 class="card-title">{{$knowledge->title}}</h5>
                        <p class="card-text">
                            {{str_limit( $knowledge->detail, 150)}}
                        </p>
                        <a href="{{url('knowledge/'.$knowledge->id)}}" class="readmore">more.. </a>
                    </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection