@extends('master')

@section('content')
<section id="portfolio" class="clearfix">
    <div class="row">
        <div class="col-md-12" style="height:10vh"></div>
    </div>
    <div class="container">

      <header class="section-header">
        <h3 class="section-title">กิจกรรม</h3>
      </header>
      <div class="row">
        <div class="col-md-12" style="height:10vh"></div>
      </div>

      <div class="row portfolio-container">
        @foreach($blogs as $blog)
        <div class="col-lg-4 col-md-6 portfolio-item filter-app">
          <div class="portfolio-wrap">
            <img src="{{$blog->cover_img}}" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>
                <a href="{{url('showblog/'.$blog->id)}}">{{$blog->title}}</a>
              </h4>
              {{-- <p>App</p> --}}
              <div>
                <a href="{{$blog->cover_img}}" data-lightbox="portfolio" data-title="App 1" class="link-preview" title="Preview"><i class="ion ion-eye"></i></a>
                <a href="{{url('showblog/'.$blog->id)}}" class="link-details" title="More.."><i class="ion ion-android-open"></i></a>
              </div>
            </div>
          </div>
        </div>
        @endforeach
      </div>

    </div>
  </section>
@endsection