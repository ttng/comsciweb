@extends('master')

@section('style')
<style>
.blog-content{
  margin-bottom: 6vh;
}
</style>
@endsection

@section('content')
<section id="portfolio" class="clearfix">
    <div class="row">
        <div class="col-md-12" style="height:10vh"></div>
    </div>
    <div class="container">
      <div class="row blog-content">
        <div class="col-md-6">
          <img src="{{$blog->cover_img}}" alt="" style="width:100%;">
        </div>
        <div class="col-md-6">
          <header class="section-header">
            <h3 class="section-title">{{$blog->title}}</h3>
          </header>
          <div>
            {{$blog->detail}}
          </div>
        </div>
      </div>
      <div class="row portfolio-container">
        <?php
          $imgs = App\Image::where('blog_id', $blog->id)->get();  
        ?>
        @foreach($imgs as $img)
        <div class="col-lg-4 col-md-6 portfolio-item filter-app">
          <div class="portfolio-wrap">
              <img src="{{$img->image}}" class="img-fluid" alt="">
              <div class="portfolio-info">
                  <div>
                    <a href="{{$img->image}}" data-lightbox="portfolio" data-title="App 1" class="link-preview" title="Preview"><i class="ion ion-eye"></i></a>
                  </div>
              </div>
          </div>
        </div>
        @endforeach
      </div>
    </div>
</section>
@endsection