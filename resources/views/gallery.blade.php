<div class="container-fluid">
        <div class="section-header">
            <a href="{{url('gallery')}}">
            <h3 class="section-title">Gallery</h3> 
            </a>
          <span class="section-divider"></span>
          {{-- <p class="section-description"></p> --}}
        </div>

        <div class="row no-gutters">
          @foreach($galleries as $gallery)
          <div class="col-lg-4 col-md-6">
            <div class="gallery-item wow fadeInUp">
              <a href="{{$gallery->image}}" class="gallery-popup">
                <img src="{{$gallery->image}}" alt="">
              </a>
            </div>
          </div>
          @endforeach
        </div>

      </div>