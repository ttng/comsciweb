<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Computer Science RERU</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="วิทยาการคอมพิวเตอร์ มหาวิทยาลัยราชภัฏร้อยเอ็ด" name="keywords">
  <meta content="วิทยาการคอมพิวเตอร์ มหาวิทยาลัยราชภัฏร้อยเอ็ด" name="description">

  <!-- Favicons -->
  <link href="/NewBiz/img/favicon.png" rel="icon">
  <link href="/NewBiz/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Kanit" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="/NewBiz/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="/NewBiz/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="/NewBiz/lib/animate/animate.min.css" rel="stylesheet">
  <link href="/NewBiz/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="/NewBiz/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="/NewBiz/lib/lightbox/css/lightbox.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="/NewBiz/css/style.css" rel="stylesheet">

  <style>
  h2, h3, h4, h5{
    font-family: 'Kanit', sans-serif;
  }
  </style>

  @yield('style')
  <!-- =======================================================
    Theme Name: NewBiz
    Theme URL: https://bootstrapmade.com/newbiz-bootstrap-business-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>

<body>

  <!--==========================
  Header
  ============================-->
  <header id="header" class="fixed-top">
    <div class="container">

      <div class="logo float-left">
        <!-- Uncomment below if you prefer to use an image logo -->
        <h1 class="text-light"><a href="#header"><span>CS RERU</span></a></h1>
      </div>

      <nav class="main-nav float-right d-none d-lg-block">
        <ul>
          <li class="menu-active"><a href="{{url('/')}}#intro">Home</a></li>
          <li><a href="{{url('/')}}#advanced-features">ปรัชญา</a></li>
          <li><a href="{{url('knowledge')}}">องค์ความรู้</a></li>
          <li><a href="{{url('getblog')}}">กิจกรรม</a></li>
          <li><a href="{{url('paper')}}">ผลงานวิชาการ</a></li>
          <li><a href="{{url('/')}}#team">บุคลากร</a></li>
          <li><a href="{{url('gallery')}}">Gallery</a></li>
          <li><a href="{{url('/')}}#contact">ติดต่อ</a></li>
        </ul>
      </nav><!-- .main-nav -->
      
    </div>
  </header><!-- #header -->

 

  <main id="main">
    @yield('content')
  </main>

  <!--==========================
    Footer
  ============================-->
  <footer id="footer">

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>CS RERU</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
          All the links in the footer should remain intact.
          You can delete the links only if you purchased the pro version.
          Licensing information: https://bootstrapmade.com/license/
          Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=NewBiz
        -->
        {{-- Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a> --}}
      </div>
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <!-- Uncomment below i you want to use a preloader -->
  <!-- <div id="preloader"></div> -->

  <!-- JavaScript Libraries -->
  <script src="/NewBiz/lib/jquery/jquery.min.js"></script>
  <script src="/NewBiz/lib/jquery/jquery-migrate.min.js"></script>
  <script src="/NewBiz/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="/NewBiz/lib/easing/easing.min.js"></script>
  <script src="/NewBiz/lib/mobile-nav/mobile-nav.js"></script>
  <script src="/NewBiz/lib/wow/wow.min.js"></script>
  <script src="/NewBiz/lib/waypoints/waypoints.min.js"></script>
  <script src="/NewBiz/lib/counterup/counterup.min.js"></script>
  <script src="/NewBiz/lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="/NewBiz/lib/isotope/isotope.pkgd.min.js"></script>
  <script src="/NewBiz/lib/lightbox/js/lightbox.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="/NewBiz/contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="/NewBiz/js/main.js"></script>

</body>
</html>
