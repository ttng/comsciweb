<div class="container-fluid">
  <div class="section-header">
      <a href="{{url('paper')}}">
      <h3 class="section-title">ผลงานวิชาการ</h3> 
      </a>
    <span class="section-divider"></span>
    {{-- <p class="section-description"></p> --}}
  </div>

  <div class="row no-gutters">
    @foreach($papers as $paper)
    <div class="col-lg-4 col-md-6">
      <div class="gallery-item wow fadeInUp text-center">
        <a href="{{url('paper/'.$paper->id)}}">
          <img src="{{$paper->cover}}" alt="" style="width:50%">
        </a>
      </div>
    </div>
    @endforeach
  </div>
  <div class="row" style="margin-top:5vh">
      <div class="col-md-12 text-right">
          <a class="cta-btn align-middle custom-btn" href="{{url('paper')}}">More..</a>
      </div>
  </div>

</div>