<div class="row">
    <div class="col-md-12 custom-header">
        <video src="/web/video/coding.mp4" autoplay muted loop id="myVideo">
        </video>
        <div class="intro-text">
          <h2 id="head1">วิทยาการคอมพิวเตอร์</h2>
          <h1 id="title1">มหาวิทยาลัยราชภัฏร้อยเอ็ด</h1>
          <a href="http://admission.reru.ac.th/" target="_blank" class="btn-get-started scrollto admission">สมัครเรียน ปีการศึกษา 2562</a>
        </div>
    
        <div class="product-screens">
    
          <div class="product-screen-1 wow fadeInUp" data-wow-delay="0.4s" data-wow-duration="0.6s">
            <img src="/web/img/title-1.png" alt="">
          </div>
    
          <div class="product-screen-2 wow fadeInUp" data-wow-delay="0.2s" data-wow-duration="0.6s">
            <img src="/web/img/title-2.png" alt="">
          </div>
    
          <div class="product-screen-3 wow fadeInUp" data-wow-duration="0.6s">
            <img src="/web/img/title-3.png" alt="">
          </div>
        </div>
    </div>
</div>