
<div class="container">
    <div class="section-header">
        <a href="{{url('getblog')}}">
            <h3 class="section-title">กิจกรรม</h3>
        </a> 
        <span class="section-divider"></span>
        <p class="section-description">กิจกรรมสาขาวิชาวิทยาการคอมพิวเตอร์และคณะเทคโนโลยีสารสนเทศ</p>
    </div>

    <div class="row">
        @foreach($blogs as $blog)
        <div class="col-md-6">
            <div class="row">
               <div class="col-md-5">
                   <a href="{{url('showblog/'.$blog->id)}}">
                       <img src="{{$blog->cover_img}}" style="height:30vh">
                   </a>
               </div>
                <div class="col-md-7">
                    <div class="box featured fadeInUp">
                        <h3>{{$blog->title}}</h3>
                        {{-- <h4><sup>$</sup>29<span> month</span></h4> --}}
                        <ul>
                            <li><i class="ion-android-checkmark-circle"></i>
                                {{str_limit( $blog->detail, 150)}}
                            </li>
                        </ul>
                        <a href="{{url('showblog/'.$blog->id)}}" class="get-started-btn">Read More</a>
                    </div> 
                </div> 
            </div>
            
            
        </div>
     
        @endforeach
    </div>
    <div class="row">
        <div class="col-md-12 text-right">
            <a class="cta-btn align-middle custom-btn" href="{{url('getblog')}}">More..</a>
        </div>
    </div>
</div>