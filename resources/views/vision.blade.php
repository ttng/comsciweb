<div class="features-row">
    <div class="container">
        <div class="row">
        <div class="col-12">
            <img class="advanced-feature-img-left" src="/web/img/vision.png" alt="">
            <div class="wow fadeInRight">
            <h2>ปรัชญา</h2>
            <i class="ion-ios-paper-outline" class="wow fadeInRight" data-wow-duration="0.5s"></i>
            <p class="wow fadeInRight" data-wow-duration="0.5s">
                  มุ่งผลิตบัณฑิตเพื่อความเป็นเลิศด้านวิชาการและวิชาชีพ มีความคิดริเริ่มสร้างสรรค์ มีคุณธรรม และมีความรับผิดชอบต่อสังคมและประเทศชาติ  
            </p>
            {{-- <i class="ion-ios-color-filter-outline wow fadeInRight" data-wow-delay="0.2s" data-wow-duration="0.5s"></i>
            <p class="wow fadeInRight" data-wow-delay="0.2s" data-wow-duration="0.5s">Quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum.</p>
            <i class="ion-ios-barcode-outline wow fadeInRight" data-wow-delay="0.4" data-wow-duration="0.5s"></i>
            <p class="wow fadeInRight" data-wow-delay="0.4s" data-wow-duration="0.5s">Quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum.</p> --}}
            </div>
        </div>
        </div>
    </div>
</div>