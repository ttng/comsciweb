<?php
  // dd($teams);
?>
    <div class="container">
      <div class="section-header">
        <h3 class="section-title">บุคลากร</h3>
        
        <span class="section-divider"></span>
        {{-- <p class="section-description">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque</p> --}}
      </div>
      <div class="row">
        <div class="col-md-12" style="height:10vh"></div>
      </div>
      <div class="row wow fadeInUp">
          <div class="col-md-4"></div>
          <div class="col-lg-4" style="border: 2px solid #1dc8cd; padding:3vh 2vh 1vh 2vh; margin-bottom: 3vh">
            <div class="member">
              <div class="pic"><img src="{{$head->image}}" alt=""></div>
         
              <h4>{{$head->name}}</h4>
              <div style="color: #00a6cd; font-size: 130%; font-family: 'Kanit', sans-serif;">ประธานหลักสูตร</div>
              <div class="social">
                <a href="{{$head->email}}"><i class="fa fa-google-plus"></i></a>
                <a href="{{$head->web}}"><i class="fa fa-home"></i></a>
              </div>
            </div>
          </div>
          <div class="col-md-4"></div>
        </div>
      <div class="row wow fadeInUp">
        @foreach($teams as $team)
        <div class="col-md-4">
          <div class="member">
            <div class="pic"><img src="{{$team->image}}" alt=""></div>
            <h4>{{$team->name}}</h4>
            <span>อาจารย์ประจำหลักสูตร</span>
            <div class="social">
                <a href="{{$team->email}}"><i class="fa fa-google-plus"></i></a>
                <a href="{{$team->web}}"><i class="fa fa-home"></i></a>
            </div>
          </div>
        </div>
        @endforeach
      </div>

    </div>