@extends('master')

@section('style')
<style>
.paper-content{
  margin-bottom: 6vh;
}
</style>
@endsection

@section('content')
<section id="portfolio" class="clearfix">
    <div class="row">
        <div class="col-md-12" style="height:10vh"></div>
    </div>
    <div class="container">
      <div class="row paper-content">
        <div class="col-md-6">
          <img src="{{$paper->cover}}" alt="" style="width:100%;">
        </div>
        <div class="col-md-6">
          <header class="section-header">
            <h3 class="section-title">{{$paper->title}}</h3>
            <h4>ผู้แต่ง : {{$paper->author}}</h4>
          </header>
          <div>
            {{$paper->detail}}
          </div>
        </div>
      </div>
  
    </div>
</section>
@endsection