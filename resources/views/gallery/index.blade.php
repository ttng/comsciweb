@extends('master')

@section('content')
<section id="portfolio" class="clearfix">
    <div class="row">
        <div class="col-md-12" style="height:10vh"></div>
    </div>
    <div class="container">

      <header class="section-header">
        <h3 class="section-title">Gallery</h3>
      </header>
      <div class="row">
        <div class="col-md-12" style="height:10vh"></div>
      </div>

      <div class="row portfolio-container">
        @foreach($galleries as $gallery)
        <div class="col-lg-4 col-md-6 portfolio-item filter-app">
          <div class="portfolio-wrap">
            <img src="{{$gallery->image}}" class="img-fluid" alt="">
            <div class="portfolio-info">
              <div>
                <a href="{{$gallery->image}}" data-lightbox="portfolio" data-title="App 1" class="link-preview" title="Preview"><i class="ion ion-eye"></i></a>
              </div>
            </div>
          </div>
        </div>
        @endforeach
      </div>

    </div>
  </section>
@endsection