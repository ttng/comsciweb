@extends('admin.layout.main')
@section('header')
    <style>
        .topic{
            font-size: 200%;
            font-family: 'Kanit', sans-serif;
            margin-bottom: 6vh;
        }
        .custom-group{
            padding-bottom: 3vh;
        }
        .add-icon{
            font-size: 200%;
            color: #1dc8cd;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="topic text-center">เพิ่มองค์ความรู้</div>
                </div>
            </div>
           <div class="row">
               <div class="col-md-2"></div>
               <div class="col-md-8">
                    <div class="ibox-content">
                        <form method="post" action="{{url('backend/knowledge')}}" class="form-horizontal" enctype= "multipart/form-data">
                            {{csrf_field()}}
                        <div class="form-group custom-group">
                            <label class="col-sm-2 control-label">ชื่อกิจกรรม</label>
                            <div class="col-sm-10">
                                <input type="text" name="title" class="form-control">
                            </div>
                        </div>
                        <div class="form-group custom-group">
                            <label class="col-sm-2 control-label">รายละเอียด</label>
                            <div class="col-sm-10">
                                <textarea name="detail" cols="80" rows="10"></textarea>
                            </div>
                        </div>
                        <div class="form-group custom-group">
                            <label class="col-sm-2 control-label">credit</label>
                            <div class="col-sm-10">
                                <input type="text" name="credit" class="form-control" placeholder="แหล่งที่มา">
                            </div>
                        </div>
                        <div class="form-group custom-group">
                            <label class="col-sm-2 control-label">cover</label>
                            <div class="col-sm-10">
                                <div class="custom-file">
                                    <input name="cover" type="file" class="custom-file-input form-control" placeholder=".jpg .png">
                                </div> 
                            </div>
                        </div>
                        <div class="form-group custom-group">
                            <label class="col-sm-2 control-label">Icon</label>
                            <div class="col-sm-6">
                                <?php
                                    $icons = App\Icon::all();
                                ?>
                                <select name="icon" id="cover" class="form-control">
                                    <option value="0">--เลือก Icon--</option>
                                    @foreach($icons as $icon)
                                        <option value="{{$icon->name}}">{{$icon->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-4 add-icon">
                                <i id="cover_icon"></i>
                            </div>
                        </div>
                        <div class="form-group custom-group text-center">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <a href="{{url('backend/knowledge')}}" class="btn btn-outline btn-warning">Cancle</a>
                        </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-2"></div>
           </div>
        </div>
    </div>
@endsection
@section('footer')
<script>
    $(document).ready(function(){
        $('#admin-knowledge-menu').addClass('active');

        $('#cover').change(function(){
            $('#cover_icon').removeClass();
            var name = $(this).val();
            $('#cover_icon').addClass(name);
            $('#cover_icon').addClass('fa');
        });
        
    });
</script>
@endsection