@extends('admin.layout.main')
@section('header')
    <link href="/inspinia/css/plugins/footable/footable.core.css" rel="stylesheet">
    <style>
        .topic{
            font-size: 200%;
            font-family: 'Kanit', sans-serif;
        }
        .huge{
            font-size: 20vh;
            color: steelblue;
        }
        .huge span{
                font-size: 6vh;
                color: grey;
        }
        .cover-img{
            width: 30vh;
        }
        .add-icon{
            font-size: 200%;
            color: #1dc8cd;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-10">
                <div class="topic">จัดการองค์ความรู้</div>
            </div>
            <div class="col-md-2">
                <div class="topic">เพิ่มองค์ความรู้</div>
            </div>
            <div class="col-md-10">
                <div class="col-md-12">
                    <div class="ibox-content">
                        <input type="text" class="form-control input-sm m-b-xs" id="filter"
                                placeholder="Search in table">
                        <table class="footable table table-stripped" data-page-size="8" data-filter=#filter>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Topic</th>
                                    <th>ภาพปก</th>
                                    <th>Icon</th>
                                    <th>วันที่</th>
                                    <th>แก้ไข</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $index = 1; ?>
                                @foreach ($knowledges as $knowledge)
                                <tr>
                                    <td>{{$index++}}</td>
                                    <td>{{$knowledge->title}}</td>
                                    <td><img src="{{$knowledge->cover}}" class="cover-img"></td>
                                    <?php
                                        $icon_name = App\Icon::find($knowledge->icon_id)->name;
                                    ?>
                                    <td><i class="add-icon fa {{$icon_name}}"></i></td>
                                    <td>{{$knowledge->created_at}}</td>
                                    <td>
                                        <a href="{{url('backend/knowledge/'.$knowledge->id.'/edit')}}" class="btn btn-outline btn-warning">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="5">
                                    <ul class="pagination pull-right"></ul>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>                        
                </div>
            </div>
            <div class="col-md-2 huge">
                <a href={{url('backend/knowledge/create')}}>
                    <i class="fa fa-mortar-board"></i>
                </a>
            </div>
        </div>
    </div>
@endsection
@section('footer')
<script src="/inspinia/js/plugins/footable/footable.all.min.js"></script>
    <script>
        $(document).ready(function(){
            $('#admin-knowledge-menu').addClass('active');
            // $('#admin-home-menu').closest('ul').toggleClass('collapse');
            $('.footable').footable();
            $('.footable2').footable();
        });
    </script>
@endsection