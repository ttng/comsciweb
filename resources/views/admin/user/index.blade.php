@extends('admin.layout.main')
@section('header')
    <link href="/inspinia/css/plugins/footable/footable.core.css" rel="stylesheet">
    <style>
        .topic{
            font-size: 200%;
            font-family: 'Kanit', sans-serif;
        }
        .huge{
            font-size: 20vh;
            color: steelblue;
        }
        .huge span{
                font-size: 6vh;
                color: grey;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-9">
                <div class="topic">จัดการผู้ใช้งานระบบ</div>
            </div>
            <div class="col-md-3">
                <div class="topic">เพิ่มบุคลากร</div>
            </div>
            <div class="col-md-9">
                <div class="col-md-12">
                    <div class="ibox-content">
                        <input type="text" class="form-control input-sm m-b-xs" id="filter"
                                placeholder="Search in table">
                        <table class="footable table table-stripped" data-page-size="8" data-filter=#filter>
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>ชื่อ-สกุล</th>
                                    <th>ตำแหน่ง</th>
                                    <th>สังกัด</th>
                                    <th>โทรศัพท์</th>
                                    <th>ประวัติการซ่อม</th>
                                    <th>Role</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $index = 1; ?>
                                @foreach ($users as $user)
                                <tr>
                                    <td>{{$index++}}</td>
                                    <td>{{$user->member->name}}</td>
                                    <td>{{$user->member->position}}</td>
                                    <td>{{$user->member->department->name}}</td>
                                    <td>{{$user->member->phone}}</td>
                                    <td>
                                        <a href="{{url('admin-user/'.$user->id)}}" class="btn btn-outline btn-warning">
                                            <i class="fa fa-list"></i>
                                        </a>
                                    </td>
                                    <td>{{$user->role->name}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="5">
                                    <ul class="pagination pull-right"></ul>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>                        
                </div>
            </div>
            <div class="col-md-3 huge">
                <a href={{url('/admin-user/create')}}>
                    <i class="fas fa-users"></i>
                </a>
            </div>
        </div>
    </div>
@endsection
@section('footer')
<script src="/inspinia/js/plugins/footable/footable.all.min.js"></script>
    <script>
        $(document).ready(function(){
            $('#admin-member-menu').addClass('active');
            // $('#admin-home-menu').closest('ul').toggleClass('collapse');
            $('.footable').footable();
            $('.footable2').footable();
        });
    </script>
@endsection