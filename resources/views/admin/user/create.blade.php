@extends('admin.layout.main')
@section('header')
    <style>
        .topic{
            font-size: 200%;
            font-family: 'Kanit', sans-serif;
            margin-bottom: 6vh;
        }
        .custom-group{
            padding-bottom: 3vh;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="topic text-center">เพิ่มบุคลากร</div>
                </div>
            </div>
           <div class="row">
               <div class="col-md-2"></div>
               <div class="col-md-8">
                    <div class="ibox-content">
                        {!! Form::open(['method' => 'POST', 'url' => '/admin-user', 'class' => 'form-horizontal']) !!}
                        <div class="form-group custom-group">
                            <label class="col-sm-2 control-label">Username</label>
                            <div class="col-sm-10">
                                {!! Form::input('text', 'username', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group custom-group">
                            <label class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-10">
                                {!! Form::email('email', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group custom-group">
                            <label class="col-sm-2 control-label">Password</label>
                            <div class="col-sm-10">
                                {!! Form::password('password', ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group custom-group">
                            <label class="col-sm-2 control-label">ชื่อ-สกุล</label>
                            <div class="col-sm-10">
                                {!! Form::input('text', 'name', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group custom-group">
                            <label class="col-sm-2 control-label">ตำแหน่ง</label>
                            <div class="col-sm-10">
                                {!! Form::select('position', ['อาจารย์', 'เจ้าหน้าที่'], null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group custom-group">
                            <label class="col-sm-2 control-label">โทรศัพท์</label>
                            <div class="col-sm-10">
                                {!! Form::input('text', 'phone', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group custom-group">
                            <label class="col-sm-2 control-label">สังกัด</label>
                            <div class="col-sm-10">
                                {!! Form::select('department_id', $departments, null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group custom-group">
                            {{ Form::submit('Save', array('class' => 'btn btn-block btn-outline btn-primary')) }}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="col-md-2"></div>
           </div>
        </div>
    </div>
@endsection
@section('footer')
<script>
    $(document).ready(function(){
        $('#admin-member-menu').addClass('active');
    });
</script>
@endsection