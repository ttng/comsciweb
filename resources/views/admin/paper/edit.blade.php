@extends('admin.layout.main')
@section('header')
    <style>
        .topic{
            font-size: 200%;
            font-family: 'Kanit', sans-serif;
            margin-bottom: 6vh;
        }
        .custom-group{
            padding-bottom: 3vh;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="topic text-center">แก้ไขผลงานวิชาการ</div>
                </div>
            </div>
           <div class="row">
               <div class="col-md-2"></div>
               <div class="col-md-8">
                    <div class="ibox-content">
                        <form method="post" action="{{url('backend/paper/'.$paper->id)}}" class="form-horizontal" enctype= "multipart/form-data">
                            {{ method_field('PUT') }}
                            {{csrf_field()}}
                        <div class="form-group custom-group">
                            <label class="col-sm-2 control-label">ชื่อผลงาน</label>
                            <div class="col-sm-10">
                                <input type="text" name="title" class="form-control" value="{{$paper->title}}">
                            </div>
                        </div>
                        <div class="form-group custom-group">
                            <label class="col-sm-2 control-label">ผู้แต่ง</label>
                            <div class="col-sm-10">
                                <input type="text" name="author" class="form-control" value="{{$paper->author}}">
                            </div>
                        </div>
                        <div class="form-group custom-group">
                            <label class="col-sm-2 control-label">รายละเอียด</label>
                            <div class="col-sm-10">
                                <textarea name="detail" cols="80" rows="10">
                                    {{$paper->detail}}
                                </textarea>
                            </div>
                        </div>
                        <div class="form-group custom-group text-center">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <a href="{{url('backend/paper')}}" class="btn btn-outline btn-warning">Cancle</a>
                        </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-2"></div>
           </div>
        </div>
    </div>
@endsection
@section('footer')
<script>
    $(document).ready(function(){
        $('#admin-paper-menu').addClass('active');
    });
</script>
@endsection