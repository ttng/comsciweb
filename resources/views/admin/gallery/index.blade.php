@extends('admin.layout.main')
@section('header')
    <link href="/inspinia/css/plugins/footable/footable.core.css" rel="stylesheet">
    <style>
        .topic{
            font-size: 200%;
            font-family: 'Kanit', sans-serif;
        }
        .huge{
            font-size: 20vh;
            color: steelblue;
        }
        .huge span{
                font-size: 6vh;
                color: grey;
        }
        .cover-img{
            width: 30vh;
        }
        .add-icon{
            font-size: 200%;
            color: #1dc8cd;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-10">
                <div class="topic">จัดการ Gallery</div>
            </div>
            <div class="col-md-2">
                <div class="topic">เพิ่มภาพ</div>
            </div>
            <div class="col-md-10">
                <div class="col-md-12">
                    <div class="ibox-content">
                        <input type="text" class="form-control input-sm m-b-xs" id="filter"
                                placeholder="Search in table">
                        <table class="footable table table-stripped" data-page-size="8" data-filter=#filter>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>ภาพ</th>
                                    <th>วันที่</th>
                                    <th>ลบ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $index = 1; ?>
                                @foreach ($galleries as $gallery)
                                <tr>
                                    <td>{{$index++}}</td>
                                    <td><img src="{{$gallery->image}}" class="cover-img"></td>
                                    <td>{{$gallery->created_at}}</td>
                                    <td>
                                        <form action="{{url('backend/gallery/'.$gallery->id)}}" method="post">
                                            {{ method_field('DELETE') }}
                                            {{csrf_field()}}
                                            <button class="btn btn-outline btn-danger">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </form>                                        
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="5">
                                    <ul class="pagination pull-right"></ul>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>                        
                </div>
            </div>
            <div class="col-md-2 huge">
                <a href={{url('backend/gallery/create')}}>
                    <i class="fa fa-image"></i>
                </a>
            </div>
        </div>
    </div>
@endsection
@section('footer')
<script src="/inspinia/js/plugins/footable/footable.all.min.js"></script>
    <script>
        $(document).ready(function(){
            $('#admin-gallery-menu').addClass('active');
            // $('#admin-home-menu').closest('ul').toggleClass('collapse');
            $('.footable').footable();
            $('.footable2').footable();
        });
    </script>
@endsection