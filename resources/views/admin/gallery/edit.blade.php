@extends('admin.layout.main')
@section('header')
    <style>
        .topic{
            font-size: 200%;
            font-family: 'Kanit', sans-serif;
            margin-bottom: 6vh;
        }
        .custom-group{
            padding-bottom: 3vh;
        }
        .add-icon{
            font-size: 200%;
            color: #1dc8cd;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="topic text-center">แก้ไของค์ความรู้</div>
                </div>
            </div>
           <div class="row">
               <div class="col-md-2"></div>
               <div class="col-md-8">
                    <div class="ibox-content">
                        <form method="post" action="{{url('backend/knowledge/'.$knowledge->id)}}" class="form-horizontal" enctype= "multipart/form-data">
                            {{ method_field('PUT') }}
                            {{csrf_field()}}
                        <div class="form-group custom-group">
                            <label class="col-sm-2 control-label">ชื่อกิจกรรม</label>
                            <div class="col-sm-10">
                                <input type="text" name="title" class="form-control" value={{$knowledge->title}}>
                            </div>
                        </div>
                        <div class="form-group custom-group">
                            <label class="col-sm-2 control-label">รายละเอียด</label>
                            <div class="col-sm-10">
                                <textarea name="detail" cols="80" rows="10">
                                    {{$knowledge->detail}}
                                </textarea>
                            </div>
                        </div>
                        <div class="form-group custom-group">
                            <label class="col-sm-2 control-label">Icon</label>
                            <div class="col-sm-6">
                                <?php
                                    $icons = App\Icon::all();
                                ?>
                                <select name="icon" id="cover" class="form-control">
                                    @foreach($icons as $icon)
                                        <option value="{{$icon->name}}" @if($knowledge->icon_id == $icon->id) ? selected @endif>{{$icon->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-4 add-icon">
                                <?php
                                    $icon_name = App\Icon::find($knowledge->icon_id)->name;
                                ?>
                                <i id="cover_icon" class="fa {{$icon_name}}"></i>
                            </div>
                        </div>
                        <div class="form-group custom-group">
                            <label class="col-sm-2 control-label">credit</label>
                            <div class="col-sm-10">
                                <input type="text" name="credit" value="{{$knowledge->credit}}" class="form-control" placeholder="แหล่งที่มา">
                            </div>
                        </div>
                        <div class="form-group custom-group text-center">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <a href="{{url('backend/knowledge')}}" class="btn btn-outline btn-warning">Cancle</a>
                        </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-2"></div>
           </div>
        </div>
    </div>
@endsection
@section('footer')
<script>
    $(document).ready(function(){
        $('#admin-knowledge-menu').addClass('active');

        $('#cover').change(function(){
            $('#cover_icon').removeClass();
            var name = $(this).val();
            $('#cover_icon').addClass(name);
            $('#cover_icon').addClass('fa');
        });
    });
</script>
@endsection