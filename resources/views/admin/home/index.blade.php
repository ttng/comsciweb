@extends('admin.layout.main')
@section('header')
    <link href="/inspinia/css/plugins/footable/footable.core.css" rel="stylesheet">
    <style>
        .topic{
            font-size: 200%;
            font-family: 'Kanit', sans-serif;
        }
        .inform-btn{
            font-size: 4vh;
            border-style: solid;
            border-color: #82ffd3;
            border-width: 0.25vh;
            padding: 0 1vw 0 1vw;
            background-color: #b8fce4;
            border-radius: 1.5vh;
        }
        .inform-btn:hover{
            background-color: #05ffa6;
            color: white;
        }

    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-11">
            <div class="row">
                <div class="col-md-12">
                    <div class="topic">รายการแจ้งซ่อม</div>
                </div>
            </div>
           <div class="row">
               <div class="col-md-12">
                    <div class="ibox-content">
                        <input type="text" class="form-control input-sm m-b-xs" id="filter"
                                placeholder="Search in table">

                        <table class="footable table table-stripped" data-page-size="8" data-filter=#filter>
                            <thead>
                            <tr>
                                <th>ลำดับ</th>
                                <th>หมายเลข</th>
                                <th>รายการ</th>
                                <th>ผู้แจ้งซ่อม</th>
                                <th>สถานที่</th>
                                <th>อาการ</th>
                                <th>เวลา</th>
                                <th>ส่งซ่อม</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php $index = 1; ?>
                                @foreach ($all_informs as $inform)
                         
                                <form action="inform/{{$inform->id}}" method="post">
                                    {{ method_field('PUT') }}
                                    {{ csrf_field() }}
                                <tr>
                                    <td>{{$index++}}</td>
                                    <td>{{$inform->asset->asset_id}}</td>
                                    <td>{{$inform->asset->name}}</td>
                                    <td>{{$inform->user->member->name}} ({{$inform->user->member->phone}})</td>
                                    <td>{{$inform->asset->building->name}} ห้อง{{$inform->asset->room->name}}</td>
                                    <td>{{$inform->inform_detail}}</td>
                                    <td>{{$inform->created_at}}</td>
                                    <td><button class="inform-btn" type="submit" value="Submit"><i class="fas fa-wrench"></i></button></td>
                                </tr>
                                </form>
                                @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="5">
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                        <div class="text-center">
                            {{ $all_informs->links() }}
                        </div>
                    </div>
               </div>
           </div>
        </div>
    </div>
@endsection
@section('footer')

    <!-- FooTable -->
    <script src="/inspinia/js/plugins/footable/footable.all.min.js"></script>

    <script>
        $(document).ready(function(){
            $('#admin-inform-menu').addClass('active');
            // $('#admin-home-menu').closest('ul').toggleClass('collapse');
            $('.footable').footable();
            $('.footable2').footable();

        });
    </script>
   
@endsection