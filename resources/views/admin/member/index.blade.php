@extends('admin.layout.main')
@section('header')
    <link href="/inspinia/css/plugins/footable/footable.core.css" rel="stylesheet">
    <style>
        .topic{
            font-size: 200%;
            font-family: 'Kanit', sans-serif;
        }
        .huge{
            font-size: 20vh;
            color: steelblue;
        }
        .huge span{
                font-size: 6vh;
                color: grey;
        }
        .member-img{
            width: 24vh;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-10">
                <div class="topic">จัดการบุคลากร</div>
            </div>
            <div class="col-md-2">
                <div class="topic">เพิ่มบุคลากร</div>
            </div>
            <div class="col-md-10">
                <div class="col-md-12">
                    <div class="ibox-content">
                        <input type="text" class="form-control input-sm m-b-xs" id="filter"
                                placeholder="Search in table">
                        <table class="footable table table-stripped" data-page-size="8" data-filter=#filter>
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>ชื่อ-สกุล</th>
                                    <th>ตำแหน่ง</th>
                                    <th>Email</th>
                                    <th>Web</th>
                                    <th>Cover</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $index = 1; ?>
                                @foreach ($members as $member)
                                <tr>
                                    <td>{{$index++}}</td>
                                    <td>{{$member->name}}</td>
                                    <?php
                                        $position = '';
                                        if($member->position == '1'){
                                            $position = 'อาจารย์ประจำหลักสูตร';
                                        }else{
                                            $position = 'ประธานหลักสูตร';
                                        }
                                    ?>
                                    <td>{{$position}}</td>
                                    <td>{{$member->email}}</td>
                                    <td>{{$member->web}}</td>
                                    <td><img src="{{$member->image}}" class="member-img"></td>
                                    <td>
                                        <a href="{{url('backend/member/'.$member->id.'/edit')}}" class="btn btn-outline btn-warning">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="5">
                                    <ul class="pagination pull-right"></ul>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>                        
                </div>
            </div>
            <div class="col-md-2 huge">
                <a href={{url('backend/member/create')}}>
                    <i class="fa fa-users"></i>
                </a>
            </div>
        </div>
    </div>
@endsection
@section('footer')
<script src="/inspinia/js/plugins/footable/footable.all.min.js"></script>
    <script>
        $(document).ready(function(){
            $('#admin-member-menu').addClass('active');
            // $('#admin-home-menu').closest('ul').toggleClass('collapse');
            $('.footable').footable();
            $('.footable2').footable();
        });
    </script>
@endsection