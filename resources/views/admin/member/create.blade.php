@extends('admin.layout.main')
@section('header')
    <style>
        .topic{
            font-size: 200%;
            font-family: 'Kanit', sans-serif;
            margin-bottom: 6vh;
        }
        .custom-group{
            padding-bottom: 3vh;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="topic text-center">เพิ่มบุคลากร</div>
                </div>
            </div>
           <div class="row">
               <div class="col-md-2"></div>
               <div class="col-md-8">
                    <div class="ibox-content">
                        <form method="post" action="{{url('backend/member')}}" class="form-horizontal" enctype= "multipart/form-data">
                            {{csrf_field()}}
                        <div class="form-group custom-group">
                            <label class="col-sm-2 control-label">ชื่อ</label>
                            <div class="col-sm-10">
                                <input type="text" name="name" class="form-control">
                            </div>
                        </div>
                        <div class="form-group custom-group">
                            <label class="col-sm-2 control-label">ตำแหน่ง</label>
                            <div class="col-sm-10">
                                <select name="position" class="form-control">
                                    <option value="1">อาจารย์ประจำหลักสูตร</option>
                                    <option value="2">ประธานหลักสูตร</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group custom-group">
                            <label class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-10">
                                <input type="text" name="email" class="form-control">
                            </div>
                        </div>
                        <div class="form-group custom-group">
                            <label class="col-sm-2 control-label">Website</label>
                            <div class="col-sm-10">
                                <input type="text" name="web" class="form-control">
                            </div>
                        </div>
                        <div class="form-group custom-group">
                            <label class="col-sm-2 control-label">รูป</label>
                            <div class="col-sm-10">
                                <div class="custom-file">
                                    <input name="image" type="file" class="custom-file-input form-control" placeholder=".jpg .png">
                                </div> 
                            </div>
                        </div>
                        <div class="form-group custom-group text-center">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <a href="{{url('backend/member')}}" class="btn btn-outline btn-warning">Cancle</a>
                        </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-2"></div>
           </div>
        </div>
    </div>
@endsection
@section('footer')
<script>
    $(document).ready(function(){
        $('#admin-member-menu').addClass('active');
    });
</script>
@endsection