
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>RERU Service</title>

    <link href="/inspinia/css/bootstrap.min.css" rel="stylesheet">
    {{-- <link href="/fontawesome/web/css/fontawesome-all.css" rel="stylesheet"> --}}
    <link href="/NewBiz/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="/inspinia/css/animate.css" rel="stylesheet">
    <link href="/inspinia/css/style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Kanit" rel="stylesheet">
    <link href="/NewBiz/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <style>
        .main-title{
            font-family: 'kanit';
        }
    </style>
    @yield('header')
</head>

<body>

    <div id="wrapper">

    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element"> <span>
                        <img alt="image" class="img-circle" src="/inspinia/img/profile_small.jpg" />
                            </span>
                        <span class="clear"> 
                            <span class="block m-t-xs">
                                    <strong class="font-bold">{{Auth::user()->name}}</strong>
                            </span>
                        </span>
                    </div>
                    <div class="logo-element">
                        RERU
                    </div>
                </li>
                <li id="admin-blog-menu">
                    <a href={{url('/backend/blog')}}>
                        <i class="fa fa-qrcode"></i>
                        <span class="nav-label">กิจกรรม</span>
                    </a>
                </li>
                <li id="admin-paper-menu">
                    <a href={{url('/backend/paper')}}>
                        <i class="fa fa-book"></i>
                        <span class="nav-label">ผลงานวิชาการ</span>
                    </a>
                </li>
                <li id="admin-member-menu">
                    <a href={{url('backend/member')}}>
                        <i class="fa fa-sitemap"></i>
                        <span class="nav-label">บุคลากร</span>
                    </a>
                </li>
                <li id="admin-gallery-menu">
                    <a href={{url('backend/gallery')}}>
                        <i class="fa fa-image"></i>
                        <span class="nav-label">Gallery</span>
                    </a>
                </li>
                <li id="admin-knowledge-menu">
                    <a href={{url('backend/knowledge')}}>
                        <i class="fa fa-mortar-board"></i>
                        <span class="nav-label">องค์ความรู้</span>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <form role="search" class="navbar-form-custom" action="search_results.html">
                <div class="form-group">
                    <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                </div>
            </form>
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <a href="{{url('backend/getlogout')}}">Logout</a>
                </li>
            </ul>
        </nav>
</div>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h1 class="main-title">จัดการข้อมูลสาขาวิชาวิทยาการคอมพิวเตอร์</h1>
        </div>
        <div class="col-lg-3">
            <div class="text-right">
                <h1 id="timer"></h1>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeIn">
        <div class="row">
            <div class="container">
                @yield('content')
            </div>
            <div class="footer">
                <div>
                    <strong>RERU</strong> Computer Science
                </div>
            </div>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="/inspinia/js/jquery-3.3.1.min.js"></script>
    <script src="/inspinia/js/new_bootstrap/bootstrap.min.js"></script>
    <script src="/inspinia/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="/inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Peity -->
    <script src="/inspinia/js/plugins/peity/jquery.peity.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="/inspinia/js/inspinia.js"></script>
    <script src="/inspinia/js/plugins/pace/pace.min.js"></script>

    <!-- Rickshaw -->
    <script src="/inspinia/js/plugins/rickshaw/vendor/d3.v3.js"></script>
    <script src="/inspinia/js/plugins/rickshaw/rickshaw.min.js"></script>
    <script>
        var myVar = setInterval(myTimer, 1000);
        
        function myTimer() {
            var d = new Date();
            document.getElementById("timer").innerHTML = d.toLocaleTimeString();
        }
    </script>
    @yield('footer')

</body>

</html>
