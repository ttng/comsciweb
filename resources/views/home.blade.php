<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Computer Science RERU</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="วิทยาการคอมพิวเตอร์ มหาวิทยาลัยราชภัฏร้อยเอ็ด" name="keywords">
  <meta content="วิทยาการคอมพิวเตอร์ มหาวิทยาลัยราชภัฏร้อยเอ็ด" name="description">

  <!-- Favicons -->
  <link href="/web/img/favicon.png" rel="icon">
  <link href="/web/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Kanit" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700|Open+Sans:300,300i,400,400i,700,700i" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="/web/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="/web/lib/animate/animate.min.css" rel="stylesheet">
  <link href="/web/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="/web/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="/web/lib/magnific-popup/magnific-popup.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="/web/css/style.css" rel="stylesheet">

  <style>
    .custom-header{
      /* height: 20vh; */
    }
    video{
      width: 100%;
    }
    h1{
      font-family: 'Kanit', sans-serif;
      color:darkorange;
    }
    h2, h3, h4, h5{
      font-family: 'Kanit', sans-serif;
    }
    p{
      font-family: 'Kanit', sans-serif;
    }

    .admission{
      font-family: 'Kanit', sans-serif !important;
    }

    .custom-btn{
      background: linear-gradient(45deg, #1de099, #1dc8cd);
      border: 0;
      border-radius: 20px;
      padding: 8px 30px;
      color: #fff;
    }

    @media only screen and (min-width: 300px) {
      .extra-space{
        height: 0vw;
      }
      .product-screens{
        display: none;
      }
      #title1{
        font-size:5vw;
      }
      #head1{
        font-size:5vw !important;
      }
      .intro-text{
        margin-top: 20vw;
      }
      #intro{
        height: 80vw;
        background: linear-gradient(45deg, rgba(29, 224, 153, 0.8), rgba(29, 200, 205, 0.8));
      }
      .scrollto{
        font-size:5vw
      }
      .admission{
        font-size: 3vw!important;
        margin:3vw !important;
      }
      #myVideo{
        display: none;
      }
     
    }
    
    @media only screen and (min-width: 600px) {
      .extra-space{
        height: 0vw;
      }
      .product-screens{
        display: none;
      }
      #title1{
        font-size:4vw;
      }
      #head1{
        font-size:4vw !important;
      }
      .intro-text{
        margin-top: 10vw;
      }
      #intro{
        height: 80vh;
      }
      .scrollto{
        font-size:3vw
      }
      #myVideo{
        display: inline;
      }

    }
    @media only screen and (min-width: 1000px) {
      .extra-space{
        height: 8vw;
      }
      .product-screens{
        display: inline;
      }
      #intro{
        height: 100vh;
      }
      .intro-text{
        margin-top: 0;
      }
      #title1{
        font-size:3vw;
      }
      #head1{
        font-size:3vw !important;
      }
      .admission{
        font-size: 2vw!important;
        margin:2vw !important;
      }
      
    }

    @media only screen and (min-width: 1200px) {
      .extra-space{
        height: 16vw;
      }
      .product-screens{
        display: inline;
      }
      #intro{
        height: 100vh;
      }
      .intro-text{
        margin-top: 0;
      }
      #title1{
        font-size:3vw;
      }
      #head1{
        font-size:3vw !important;
      }
      .admission{
        font-size: 2vw!important;
        margin:2vw !important;
      }
      
    }

    @media only screen and (min-width: 1500px) {
      .extra-space{
        height: 20vw;
      }
      .product-screens{
        display: inline;
      }
      .scrollto{
        font-size:2vw
      }
    }

    @media only screen and (min-width: 1700px) {
      .extra-space{
        height: 25vw;
      }
      .product-screens{
        display: inline;
      }
      .scrollto{
        font-size:2vw
      }
      .product-screens{
        display: none;
      }
    }

  </style>
</head>

<body>

    <header id="header">
        <div class="container">
        
            <div id="logo" class="pull-left">
            <h1><a href="#intro" class="scrollto">CS RERU</a></h1>
            <!-- Uncomment below if you prefer to use an image logo -->
            <!-- <a href="#intro"><img src="img/logo.png" alt="" title=""></a> -->
            </div>
        
            <nav id="nav-menu-container">
            <ul class="nav-menu">
                <li class="menu-active"><a href="#intro">Home</a></li>
                <li><a href="#advanced-features">ปรัชญา</a></li>
                <li><a href="{{url('knowledge')}}">องค์ความรู้</a></li>
                <li><a href="#blog">กิจกรรม</a></li>
                <li><a href="#paper">ผลงานวิชาการ</a></li>
                <li><a href="#team">บุคลากร</a></li>
                <li><a href="#gallery">Gallery</a></li>
                <li><a href="#contact">ติดต่อ</a></li>
            </ul>
            </nav><!-- #nav-menu-container -->
        </div>
    </header>
    
    <section id="intro">
        @include('header')
    </section>
    <div class="row">
        <div class="col-md-12 extra-space"></div>
    </div>
    <main id="main">
        <section id="advanced-features"> 
            @include('vision')
        </section>
        <section id="call-to-action">
            <div class="container">
            <div class="row">
                <div class="col-lg-9 text-center text-lg-left">
                <h3 class="cta-title" style="font-size:6vh">องค์ความรู้</h3>
                <p class="cta-text">องค์ความรู้ทางวิทยาการคอมพิวเตอร์</p>
                </div>
                <div class="col-lg-3 cta-btn-container text-center">
                <a class="cta-btn align-middle" href="{{url('knowledge')}}">More..</a>
                </div>
            </div>
            </div>
        </section>
        <section id="blog" class="section-bg">
            @include('blog', compact('blogs'))
        </section>
        <section id="paper">
            @include('paper', compact('papers'))
        </section>
        <section id="team" class="section-bg">
          @include('team', compact('teams', 'head'))
        </section>
        <section id="gallery">
          @include('gallery', compact('galleries'))
        </section><!-- #gallery -->
        <section id="contact">
          @include('contact')   
        </section>
    </main>

    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 text-lg-left text-center">
                <div class="copyright">
                    Computer Science RERU
                </div>
                <div class="credits">
                    
                    <!-- Designed by <a href="https://bootstrapmade.com/"></a> -->
                </div>
                </div>
                <!-- <div class="col-lg-6">
                <nav class="footer-links text-lg-right text-center pt-2 pt-lg-0">
                    <a href="#intro" class="scrollto">Home</a>
                    <a href="#about" class="scrollto">About</a>
                    <a href="#">Privacy Policy</a>
                    <a href="#">Terms of Use</a>
                </nav>
                </div> -->
            </div>
        </div>
    </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- JavaScript Libraries -->
  <script src="/web/lib/jquery/jquery.min.js"></script>
  <script src="/web/lib/jquery/jquery-migrate.min.js"></script>
  <script src="/web/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="/web/lib/easing/easing.min.js"></script>
  <script src="/web/lib/wow/wow.min.js"></script>
  <script src="/web/lib/superfish/hoverIntent.js"></script>
  <script src="/web/lib/superfish/superfish.min.js"></script>
  <script src="/web/lib/magnific-popup/magnific-popup.min.js"></script>
 
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCSiRfXkP3WRRIYGuHVCvk_QzHfE-_iV9o&callback=myMap"></script>
  <!-- Contact Form JavaScript File -->
  <script src="/web/contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="/web/js/main.js"></script>

  <script>
      function initMap() {
        // Styles a map in night mode.
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 16.142778, lng: 103.878612},
          zoom: 17,
          
          styles: [
            {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
            {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
            {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
            {
              featureType: 'administrative.locality',
              elementType: 'labels.text.fill',
              stylers: [{color: '#d59563'}]
            },
            {
              featureType: 'poi',
              elementType: 'labels.text.fill',
              stylers: [{color: '#d59563'}]
            },
            {
              featureType: 'poi.park',
              elementType: 'geometry',
              stylers: [{color: '#263c3f'}]
            },
            {
              featureType: 'poi.park',
              elementType: 'labels.text.fill',
              stylers: [{color: '#6b9a76'}]
            },
            {
              featureType: 'road',
              elementType: 'geometry',
              stylers: [{color: '#38414e'}]
            },
            {
              featureType: 'road',
              elementType: 'geometry.stroke',
              stylers: [{color: '#212a37'}]
            },
            {
              featureType: 'road',
              elementType: 'labels.text.fill',
              stylers: [{color: '#9ca5b3'}]
            },
            {
              featureType: 'road.highway',
              elementType: 'geometry',
              stylers: [{color: '#746855'}]
            },
            {
              featureType: 'road.highway',
              elementType: 'geometry.stroke',
              stylers: [{color: '#1f2835'}]
            },
            {
              featureType: 'road.highway',
              elementType: 'labels.text.fill',
              stylers: [{color: '#f3d19c'}]
            },
            {
              featureType: 'transit',
              elementType: 'geometry',
              stylers: [{color: '#2f3948'}]
            },
            {
              featureType: 'transit.station',
              elementType: 'labels.text.fill',
              stylers: [{color: '#d59563'}]
            },
            {
              featureType: 'water',
              elementType: 'geometry',
              stylers: [{color: '#17263c'}]
            },
            {
              featureType: 'water',
              elementType: 'labels.text.fill',
              stylers: [{color: '#515c6d'}]
            },
            {
              featureType: 'water',
              elementType: 'labels.text.stroke',
              stylers: [{color: '#17263c'}]
            }
          ]
        });
      }
      var myLatLng = {lat: 16.142429, lng: 103.878451};
      var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: 'Hello World!'
      });
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBlHv8CIzG5mTRefJkU0Yl2-z9lLz4aSqE&callback=initMap" async defer></script>
</body>
</html>
