<div class="container">
    <div class="section-header">
        <h3 class="section-title">ติดต่อ</h3> 
      <span class="section-divider"></span>
      {{-- <p class="section-description"></p> --}}
    </div>
        <div class="row wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">

          <div class="col-md-4">
            <div class="info">
              <div>
                <i class="ion-ios-location-outline"></i>
                <p>
                  คณะเทคโนโลยีสารสนเทศ มหาวิทยาลัยราชภัฏร้อยเอ็ด<br>
                  113 หมู่ 12 ตำบลเกาะแก้ว อำเภอเสลภูมิ<br>
                  จังหวัดร้อยเอ็ด 45120
                </p>
              </div>

              <div>
                <i class="ion-social-facebook-outline"></i>
                <p>www.facebook.com/ITRERU</p>
              </div>

              <div>
                <i class="ion-ios-telephone-outline"></i>
                <p>043-556001-8</p>
              </div>

            </div>
          </div>

          <div class="col-md-8">
              {{-- <div id="googleMap" style="width:100%;height:400px;"></div> --}}
              <div id="map" style="width:100%;height:400px;"></div>
          </div>

        </div>

      </div>